#include <HCSR04.h>
 
#define pino_trigger 4
#define pino_echo 5
 
UltraSonicDistanceSensor distanceSensor(pino_trigger, pino_echo);

void setup()
{
  Serial.begin(9600);
}
 
void loop()
{

  double distancia = distanceSensor.measureDistanceCm();

  if(distancia != -1.00){
    Serial.print(distancia);
    Serial.println(";");
    delay(1000);    
  }
}
