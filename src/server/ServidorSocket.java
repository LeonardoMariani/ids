package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

import cliente.ClienteSocket;
import seguranca.CriptoAES;
import serialComm.SerialCommRead;

public class ServidorSocket extends Thread {
	
	private static SerialCommRead leitura;
	private static String portaSerial = "COM2";
	private static int BOUDRATE = 9600;
	private static int TIMEOUT = 2000;
	private CriptoAES encript;
    private static Vector CLIENTES;
    private Socket conexao;
    private String nomeCliente;
    private static List LISTA_DE_NOMES = new ArrayList();
    
    public ServidorSocket(Socket socket) {
        this.conexao = socket;
    }
    public boolean armazena(String newName){
       for (int i=0; i < LISTA_DE_NOMES.size(); i++){
         if(LISTA_DE_NOMES.get(i).equals(newName))
           return true;
       }
       LISTA_DE_NOMES.add(newName);
       return false;
    }
    public void remove(String oldName) {
       for (int i=0; i< LISTA_DE_NOMES.size(); i++){
         if(LISTA_DE_NOMES.get(i).equals(oldName))
           LISTA_DE_NOMES.remove(oldName);
       }
    }
    
    public static void main(String args[]) {

        boolean flagSerial = true;
        CLIENTES = new Vector();
        try {
            ServerSocket server = new ServerSocket(5555);
            System.out.println("ServidorSocket rodando na porta 5555");
            if(flagSerial){
            	initLeituraSerial();
            	flagSerial = false;                    
            }
            while (true) {

                Socket conexao = server.accept();
                Thread t = new ServidorSocket(conexao);
                t.start();
            }
        } catch (IOException e) {
            System.out.println("IOException: " + e);
        }
    }
    private static void initLeituraSerial() {

        try {
			leitura = new SerialCommRead();
			leitura.init(portaSerial, BOUDRATE, TIMEOUT);
			
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

    public void run()
    {    	
    	String msgEncript;
    	encript = new CriptoAES();    			

        try {

            BufferedReader entrada = 
				new BufferedReader(new InputStreamReader(this.conexao.getInputStream()));
            
			PrintStream saida = new PrintStream(this.conexao.getOutputStream());
			
			msgEncript = entrada.readLine();
												
            //this.nomeCliente = encript.decrypt(msgEncript.getBytes("UTF-8"));
			this.nomeCliente = msgEncript;
            if (armazena(this.nomeCliente)){
              saida.println("Este nome ja existe! Conecte novamente com outro Nome.");
              CLIENTES.add(saida);
              //fecha a conexao com este cliente
              this.conexao.close();
              return;
            } else {
               //mostra o nome do cliente conectado ao servidor
               System.out.println(this.nomeCliente + " : Conectado ao Servidor!");
            }
            //igual a null encerra a execu��o
            if (this.nomeCliente == null) {
                return;
            }
            //adiciona os dados de saida do cliente no objeto CLIENTES
            CLIENTES.add(saida);
            //recebe a mensagem do cliente
            String msg = entrada.readLine();
            byte[] msgEncrypt;
            String msgEcryptString;
            
            msgEncrypt = encript.encrypt(msg);
            msgEcryptString = new String(msgEncrypt, "UTF-8");
            // Verificar se linha � null (conex�o encerrada)
            // Se n�o for nula, mostra a troca de mensagens entre os CLIENTES
            //Caso o cliente digite exit ele sera deslogado
            while (msg != null && !(msg.trim().equals("exit")))
            {            	                
                if(msg.trim().equals("dados")){
                	leitura.setEscrever(false);
                	getDados(saida);
                	leitura.setEscrever(true);                	
                }
            	// reenvia a linha para todos os CLIENTES conectados
                
                sendToAll(saida, " escreveu: ", msgEcryptString);
                System.out.print("Mensagem recebida: ");
                System.out.println(encript.decrypt(msgEncrypt));
                
                // espera por uma nova linha.
                msg = entrada.readLine();
                msgEncrypt = encript.encrypt(msg);
                msgEcryptString = new String(msgEncrypt, "UTF-8");
            }
            //se cliente enviar linha em branco, mostra a saida no servidor
            System.out.println(this.nomeCliente + " saiu do bate-papo!");
            //se cliente enviar linha em branco, servidor envia 
			// mensagem de saida do chat aos CLIENTES conectados
            sendToAll(saida, " saiu", " do bate-papo!");
            //remove nome da lista
            remove(this.nomeCliente);
            //exclui atributos setados ao cliente
            CLIENTES.remove(saida);
            //fecha a conexao com este cliente
            this.conexao.close();
        } catch (Exception e) {
            // Caso ocorra alguma excess�o de E/S, mostre qual foi.
            System.out.println("Falha na Conexao... .. ."+" IOException: " + e);
        }
    }

	private void getDados(PrintStream saida) throws IOException {
		String dados;
		byte[] dadosAES;
		//Pega os ultimos 5 registros de dados do .txt
		dados = leitura.getDados();	
		
		//encriptografia os dados
		try {
			dadosAES = encript.encrypt(dados);
			
		} catch (Exception e) {
			dadosAES = null;
			e.printStackTrace();
		}
		
		//envia para o cliente que solicitou
		sendDados(saida, dados);							
	}
	public void sendToAll(PrintStream saida, String acao, String msg) throws IOException {
        Enumeration enumClientes = CLIENTES.elements();
        
        while (enumClientes.hasMoreElements()) {
            // obt�m o fluxo de sa�da de um dos CLIENTES
            PrintStream chat = (PrintStream) enumClientes.nextElement();
            // envia para todos, menos para o pr�prio usu�rio
            if (chat != saida) {
                chat.println(this.nomeCliente + acao + msg);
            }
        }
      }
	public void sendDados(PrintStream saida, String msg){
		Enumeration enumClientes = CLIENTES.elements();
		
		while(enumClientes.hasMoreElements()){
			PrintStream chat = (PrintStream) enumClientes.nextElement();
			
			if(chat == saida){
				chat.println("Dados: " + msg);
				break;
			}
		}
	}
}