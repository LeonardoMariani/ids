package arquivo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class GerenciadorDeArquivo {
	
	private String path;
	private File arquivo;
	private BufferedWriter bw;
	private BufferedReader br;
	
	public GerenciadorDeArquivo(String path) throws IOException{
		this.path = path;
		arquivo = new File(path);
		bw = new BufferedWriter(new FileWriter(arquivo, true));
		br = new BufferedReader(new FileReader(arquivo));
	}	
	public boolean emUso(){
		
		boolean emUso = true;						
		
		if(arquivo.canWrite()){
			emUso = false;
		}else{
			emUso = true;
		}		
		return emUso;
	}	
	public boolean escreverArquivo(String retorno){
		boolean okEscrita = false;		
		try {				
			if(!emUso()){
				bw.write(retorno);
				bw.flush();				
			}			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return okEscrita;
	}
	public String lerArquivo(){		

		int qtdStopBit = 0;
		String dados = "-1";
		
		try {
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();

		    while (line != null && qtdStopBit < 5) {
		    	
			    if(line.contains(";")){
			    	qtdStopBit++;
			    }		    	
		        sb.append(line);
		        //quebra linhas
		        //sb.append(System.lineSeparator());
		        line = br.readLine();
		    }
		    dados = sb.toString();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return dados;
	}
}
