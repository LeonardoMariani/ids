package seguranca;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.net.URLDecoder;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;


//Advanced Encryption Standard
public class CriptoAES {

	static String chaveEncrypt = "0123456789abcdef";
	static String initVector = "AAAAAAAAAAAAAAAA";
	
	
	public byte[] encrypt(String texto) throws Exception{
		
		String msgString;
		byte[] msgByte;		

		IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
		SecretKeySpec chave = new SecretKeySpec(chaveEncrypt.getBytes("UTF-8"), "AES");		
		
		Cipher encripta = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE");
		encripta.init(Cipher.ENCRYPT_MODE, chave, iv);
		
		//msgByte = encripta.doFinal(texto.getBytes("UTF-8"));
		msgByte = encripta.doFinal(texto.getBytes());
		
		//msgString = new String(encripta.doFinal(msgByte),"UTF-8");
		
		//msgByte = "./error".getBytes("UTF-8");		
		
		return msgByte;	
		
	}
	public String decrypt(byte[] textoEncriptografado) throws Exception{
				
		String msg;
		
		IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));		
		SecretKeySpec chave = new SecretKeySpec(chaveEncrypt.getBytes("UTF-8"), "AES");
		
		Cipher decripta = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE");
		decripta.init(Cipher.DECRYPT_MODE, chave, iv);
			
		msg = new String(decripta.doFinal(textoEncriptografado), "UTF-8");
			
		//msg = "./error";		
		
		return msg;		
		
	}
}
