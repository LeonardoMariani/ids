package serialComm;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.swing.JSpinner.ListEditor;

import arquivo.GerenciadorDeArquivo;

public class SerialCommRead implements Runnable, SerialPortEventListener{

	private String porta;
	private int boudrate;
	private int timeout;
	private boolean escrita;
	private boolean leitura;
	private CommPortIdentifier comPortID;
	private boolean idPortaOk;
	private boolean portaOk;
	private SerialPort serialPort;
	private OutputStream saida;
	private InputStream entrada;
	private Thread threadLeitura;
	private GerenciadorDeArquivo fileManager;
	private static String path = "C:/git/Java/chat/leitura.txt";
	private ArrayList<String> listaDistancia = new ArrayList<>();
	private boolean podeEscrever;
	
	public SerialCommRead() throws IOException{

		fileManager = new GerenciadorDeArquivo(path);
		escrita = false;
		leitura = false;
		idPortaOk = false;
		podeEscrever = true;
	}
	@Override
	public void serialEvent(SerialPortEvent ev) {
//	    switch (ev.getEventType()) {
//	    case SerialPortEvent.BI:
//	    case SerialPortEvent.OE:
//	    case SerialPortEvent.FE:
//	    case SerialPortEvent.PE:
//	    case SerialPortEvent.CD:
//	    case SerialPortEvent.CTS:
//	    case SerialPortEvent.DSR:
//	    case SerialPortEvent.RI:
//	    case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
//	        break;
//	    case SerialPortEvent.DATA_AVAILABLE:
//	                   //Se tiver dados disponíveis
//	        byte[] readBuffer = new byte[6];
//	        try {
//	           int numBytes = 0;
////	            while (entrada.available() > 0) {
////	                numBytes = entrada.read(readBuffer);
////	            }
////	            String result  = new String(readBuffer);
//	            
////	            if(result.contains("<")){
////	            	while(!result.contains(">")){
////	            		System.out.println(result.toString());
////	            	}
////	            }
//	            
////	            listaDistancia.add(result);
////	            if(listaDistancia.size() > 9){
////	            	//fileManager.escreverArquivo(listaDistancia);
////	            	listaDistancia.clear();
////	            }
//
//	        } catch (IOException e) {
//	        }
//	        break;
//	    }
		
	}

	@Override
	public void run() {
        byte[] buffer = new byte[10];
        int len = -1;
        String retorno;
        try
        {
            while ( ( len = this.entrada.read(buffer)) > -1)
            {            	
            	retorno = new String(buffer,0,len);            		            	
            	System.out.print(retorno);
            	if(retorno.length() > 0){
            		if(podeEscrever){
            			fileManager.escreverArquivo(retorno);            			
            		}
            	}                                
            }
        }
        catch ( IOException e )
        {
            e.printStackTrace();
        } 	
	}
	
	public void habilitarLeitura() {
		leitura = true;
		escrita = false;		
	}
	public void setEscrever(boolean estado){
		this.podeEscrever = estado;
	}
	public void getPortaId() {
		try{
			
			comPortID = CommPortIdentifier.getPortIdentifier(porta);
			
			if(comPortID == null){
				System.out.println("Erro na porta");
				idPortaOk = false;
			}else{
				idPortaOk = true;
			}
		}catch (Exception e) {
			idPortaOk = false;	
			System.out.println("Erro ao localizar a porta.");
		}
		
	}
	public void abrirPorta() {
		try{

			serialPort = (SerialPort) comPortID.open("comm", timeout);
			portaOk = true;
			
			serialPort.setSerialPortParams(boudrate, serialPort.DATABITS_8, serialPort.STOPBITS_1, serialPort.PARITY_NONE);;
			serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_NONE);
		}catch (Exception e) {
			System.out.println("Erro ao abrir a porta.");
			portaOk = false;
		}
		
	}
	public void lerDados() {

		boolean eventsOk = false;
		if(leitura == true && idPortaOk == true){
			try{
				
				entrada = serialPort.getInputStream();				
				//serialPort.addEventListener(this);
				//serialPort.notifyOnDataAvailable(true);
				
				eventsOk = true;
			}catch (Exception e) {
				eventsOk = false;
				System.out.println("3");
			}
			
			if(eventsOk){
				try{
					threadLeitura = new Thread(this);
					threadLeitura.start();				
					//run();				
				}catch (Exception e) {
					System.out.println("2");
				}
			}		
		}
		
	}
	public void fecharComm(){
		try{
			serialPort.close();
			
		}catch (Exception e) {
			System.out.println("1");
		}
	}
	public void init(String porta, int boudrate, int timeout) {

		this.porta = porta;
		this.boudrate = boudrate;
		this.timeout = timeout;
		
    	habilitarLeitura();
    	getPortaId();
    	abrirPorta();
    	lerDados();	 
	}
	public String getDados() {
		String dados;
		
		dados = fileManager.lerArquivo();
		
		return dados;
	}
}
